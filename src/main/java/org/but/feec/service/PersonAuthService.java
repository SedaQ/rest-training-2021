package org.but.feec.service;

import org.but.feec.data.entity.Person;
import org.but.feec.data.repository.PersonRepository;
import org.but.feec.service.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
@Transactional
public class PersonAuthService implements UserDetailsService {

    private PasswordEncoder passwordEncoder;
    private PersonRepository personRepository;

    @Autowired
    public PersonAuthService(PasswordEncoder passwordEncoder,
                             PersonRepository personRepository) {
        this.passwordEncoder = passwordEncoder;
        this.personRepository = personRepository;
    }

    public boolean passwordMatch(String userPassword, String dbPasswordHash) {
        return passwordEncoder.matches(userPassword, dbPasswordHash);
    }

    @Override
    public UserDetails loadUserByUsername(String email) {
        Optional<Person> personOpt = personRepository.findByEmail(email);
        if (personOpt.isPresent()) {
            Person person = personOpt.get();
            UserDetails userDetails = new UserDetails() {
                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    // should not be hardcored.. join to the roles..
                    return AuthorityUtils.createAuthorityList("ADMIN");
                }

                @Override
                public String getPassword() {
                    return person.getPwd();
                }

                @Override
                public String getUsername() {
                    return person.getEmail();
                }

                @Override
                public boolean isAccountNonExpired() {
                    return false;
                }

                @Override
                public boolean isAccountNonLocked() {
                    return false;
                }

                @Override
                public boolean isCredentialsNonExpired() {
                    return false;
                }

                @Override
                public boolean isEnabled() {
                    return false;
                }
            };
            return userDetails;
        } else {
            throw new ResourceNotFoundException("User was not found.");
        }
    }
}
