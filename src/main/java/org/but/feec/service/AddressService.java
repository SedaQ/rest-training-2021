package org.but.feec.service;

import org.but.feec.api.AddressDetailedViewDto;
import org.but.feec.data.entity.Address;
import org.but.feec.data.repository.AddressRepository;
import org.but.feec.service.mappers.AddressMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressService {
    private AddressRepository addressRepository;
    private AddressMapper addressMapper;

    @Autowired
    public AddressService(AddressRepository addressRepository,
                          AddressMapper addressMapper) {
        this.addressRepository = addressRepository;
        this.addressMapper = addressMapper;
    }

    public AddressDetailedViewDto findAddressById(Long id) {
        Address address = addressRepository.findByIdDetailedView(id)
                .orElseThrow(
                        () -> new RuntimeException("Address was not found."));
        return addressMapper.mapToDetailedView(address);
    }
}
