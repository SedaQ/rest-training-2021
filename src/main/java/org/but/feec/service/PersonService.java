package org.but.feec.service;

import org.but.feec.service.exceptions.ResourceNotFoundException;
import org.but.feec.service.mappers.PersonMapper;
import org.but.feec.api.PersonBasicViewDto;
import org.but.feec.api.PersonCreateDto;
import org.but.feec.api.PersonDetailedViewDto;
import org.but.feec.data.entity.Person;
import org.but.feec.data.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PersonService {

    private PersonRepository personRepository;
    private PersonMapper personMapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public PersonService(PersonRepository personRepository,
                         PersonMapper personMapper,
                         PasswordEncoder passwordEncoder) {
        this.personRepository = personRepository;
        this.personMapper = personMapper;
        this.passwordEncoder = passwordEncoder;
    }

    public void createPerson(PersonCreateDto personCreateDto) {
        Person person = personMapper.mapToPerson(personCreateDto);
        person.setPwd(passwordEncoder.encode(person.getPwd()));
        personRepository.save(person);
    }

    @Transactional
//    @PreAuthorize("hasRole('ROLE_READER')")
    public Page<PersonBasicViewDto> findAll(Pageable pageable) {
        Page<Person> persons = personRepository.findAll(pageable);
        return personMapper.mapToPageDto(persons);
    }

    @Transactional
    public PersonDetailedViewDto findById(Long id) {
        Person person = personRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Person with id: " + id + " was not found."));
        return personMapper.mapToDetailView(person);
    }


    // ModelMapper, Dozer -> uses reflection
    // MapStruct -> uses "manual" mapping
}
