package org.but.feec.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.but.feec.service.PersonService;
import org.but.feec.api.PersonBasicViewDto;
import org.but.feec.api.PersonCreateDto;
import org.but.feec.api.PersonDetailedViewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.validation.Valid;

@Api(value = "/persons",
        consumes = MediaType.APPLICATION_JSON_VALUE,
        produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping(path = "/persons")
public class PersonRestController {

    private PersonService personService;

    @Autowired
    public PersonRestController(PersonService personService) {
        this.personService = personService;
    }


    @PostMapping
    public void createPerson(
            @RequestBody @Valid PersonCreateDto personCreateDto) {
        personService.createPerson(personCreateDto);
    }

    @GetMapping
    public Page<PersonBasicViewDto> findAll(Pageable pageable) {
        return personService.findAll(pageable);
    }

    /**
     * HTTP GET ~/persons/{id}
     *
     * @return PersonDetailedViewDto
     */
    @ApiOperation(
            httpMethod = "GET",
            value = "Find person by id",
            response = PersonDetailedViewDto.class,
            nickname = "findPersonById"
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "OK."),
                    @ApiResponse(code = 404, message = "Not found.")
            }
    )
    @GetMapping(path = "/{id}")
    public PersonDetailedViewDto findPersonById(@PathVariable("id") Long id) {
        return personService.findById(id);
    }

}
