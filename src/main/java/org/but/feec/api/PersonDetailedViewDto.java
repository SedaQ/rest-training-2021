package org.but.feec.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "PersonDetailedViewDto", description = "Detailed view of person in the system.")
public class PersonDetailedViewDto {
    @ApiModelProperty(value = "id", notes = "Id of the person", example = "1L")
    private Long id;
    @ApiModelProperty(value = "email", notes = "email of the person", example = "pavelseda@email.cz")
    private String email;
    @ApiModelProperty(value = "first_name", notes = "first name of the person", example = "Pavel")
    private String firstName;
    private String nickname;
    private String pwd;
    private String surname;
    private PersonAddress address;

    public PersonDetailedViewDto() {
    }

    public PersonDetailedViewDto(Long id, String email) {
        this.id = id;
        this.email = email;
    }

    public PersonAddress getAddress() {
        return address;
    }

    public void setAddress(PersonAddress address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PersonDetailedViewDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", nickname='" + nickname + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
