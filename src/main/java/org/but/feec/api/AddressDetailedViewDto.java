package org.but.feec.api;

import java.util.ArrayList;
import java.util.List;

public class AddressDetailedViewDto {
    private Long id;
    private String city;
    private String houseNumber;
    private String street;
    private String zipCode;
    private List<AddressPersons> persons = new ArrayList<>();

    public List<AddressPersons> getPersons() {
        return persons;
    }

    public void setPersons(List<AddressPersons> persons) {
        this.persons = persons;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return "AddressDetailedViewDto{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", houseNumber='" + houseNumber + '\'' +
                ", street='" + street + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
