package org.but.feec.api;

import io.swagger.annotations.ApiModelProperty;

public class PersonBasicViewDto {
    @ApiModelProperty(value = "id", notes = "Id of the person", example = "1L")
    private Long id;
    @ApiModelProperty(value = "email", notes = "email of the person", example = "pavelseda@email.cz")
    private String email;
    @ApiModelProperty(value = "first_name", notes = "first name of the person", example = "Pavel")
    private String firstName;
    private String surname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "PersonBasicViewDto{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
