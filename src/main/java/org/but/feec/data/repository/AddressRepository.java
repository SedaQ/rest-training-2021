package org.but.feec.data.repository;

import org.but.feec.data.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository
        extends JpaRepository<Address, Long> {

    @Query("SELECT a FROM Address a WHERE a.id = :id")
    Optional<Address> findById(Long id);

    @Query("SELECT a FROM Address a JOIN FETCH a.persons p WHERE a.id = :id")
    Optional<Address> findByIdDetailedView(Long id);
}
