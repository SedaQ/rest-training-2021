package org.but.feec.data.repository;

import org.but.feec.data.entity.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    @Query("SELECT p FROM Person p WHERE p.email = :email")
    Optional<Person> findByEmail(String email);

    @Query("SELECT p FROM Person p")
    Page<Person> findAll(Pageable pageable);

    @Query("SELECT p FROM Person p JOIN FETCH p.address WHERE p.id = :id")
    Optional<Person> findById(Long id);

}
