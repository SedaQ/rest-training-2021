package org.but.feec.security;

import org.but.feec.data.entity.Person;
import org.but.feec.service.PersonAuthService;
import org.but.feec.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {

    @Autowired
    private PersonAuthService personAuthService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String email = authentication.getName();
        String pwd = authentication.getCredentials().toString();

        UserDetails userDetails = personAuthService.loadUserByUsername(email);

        if (!personAuthService.passwordMatch(pwd, userDetails.getPassword())) {
            throw new BadCredentialsException("Provided credentials are not valid.");
        }
        return new UsernamePasswordAuthenticationToken(email, pwd, userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
